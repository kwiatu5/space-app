import { SpaceShip } from './space-ship';
import { Pilot } from './pilot';

export class Orzel3Ship extends SpaceShip {
  health = 30;

  /**
   *
   */
  constructor(pilot?: Pilot) {
    super('Orzeł 3', '/assets/spaceship.jpg', pilot);
  }
}
