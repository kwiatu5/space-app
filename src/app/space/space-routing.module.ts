import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HangarComponent } from './hangar/hangar.component';
import { PilotFormComponent } from './pilot-form/pilot-form.component';
import { PilotResolver } from './pilot-resolver';


const routes: Routes = [
  { path: 'space', component: HangarComponent },
  { path: 'space/pilots/:id', component: PilotFormComponent, resolve: { pilot: PilotResolver } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpaceRoutingModule { }
