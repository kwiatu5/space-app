import { PilotValidators } from './pilot-validators';
import { FormControl } from '@angular/forms';
import { AjaxResponse, ajax } from 'rxjs/ajax';
import { of } from 'rxjs';

fdescribe('PilotValidators', () => {
  it('should create an instance', () => {
    expect(new PilotValidators()).toBeTruthy();
  });
});

fdescribe('pilotName', () => {
  it('should return null string', () => {
    const ctrl = new FormControl();
    expect(PilotValidators.pilotName(ctrl)).toBeNull();
  });
});

fdescribe('pilotForbidden', () => {
  describe('when value is empty', () => {
    it('should return observable with null', () => {
      const control = new FormControl('');
      PilotValidators.pilotForbidden(control)
        .subscribe((result) => expect(result).toBeNull());
    });
  });
  describe('when value is forbidden', () => {
    it('should return observable with validation object', () => {
      const control = new FormControl('Ciapciak');
      const response = { response: [{ name: 'Ciapciak' }] } as AjaxResponse;
      spyOn(ajax, 'get').and.returnValue(of(response));
      PilotValidators.pilotForbidden(control)
        .subscribe((result) => expect(result).toEqual({ pilotForbidden: true }));
    });
  });
  describe('when value is not forbidden', () => {
    it('should return observable with null', () => {
      const control = new FormControl('Adama');
      const response = {response: []} as AjaxResponse;
      spyOn(ajax, 'get').and.returnValue(of(response));
      PilotValidators.pilotForbidden(control)
        .subscribe((result) => expect(result).toBeNull());
    });
  });
});
