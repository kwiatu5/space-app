import { SpaceShip } from './space-ship';
import { Pilot } from './pilot';

export class BomberShip extends SpaceShip {
  health = 80;
  /**
   *
   */
  constructor(pilot?: Pilot) {
    super('PrzewozOsob', '/assets/przewoz-osob.jpg', pilot);
  }
}
