import { SpaceShip } from './space-ship';
import { Pilot } from './pilot';

export class FighterShip extends SpaceShip {
  health = 50;
  /**
   *
   */
  constructor(pilot?: Pilot) {
    super('Orzeł 1', '/assets/orzel1.png', pilot);
  }
}
