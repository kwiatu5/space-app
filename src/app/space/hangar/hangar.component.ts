import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FighterShip } from '../fighter-ship';
import { SpaceShip } from '../space-ship';
import { BomberShip } from '../bomber-ship';
import { Orzel3Ship } from '../orzel3-ship';
import { Pilot } from '../pilot';
import { PilotRoomComponent } from '../pilot-room/pilot-room.component';
import { SpaceShipService } from '../space-ship.service';

@Component({
  selector: 'app-hangar',
  templateUrl: './hangar.component.html',
  styleUrls: ['./hangar.component.css']
})
export class HangarComponent implements OnInit {

  spaceShips = this.spaceShipService.hangarShips;
  selectedPilot: Pilot = null;
  @ViewChild(PilotRoomComponent, { static: false }) pilotRoom: PilotRoomComponent;

  constructor(private spaceShipService: SpaceShipService) { }

  ngOnInit() {
    // this.spaceShips.push(new FighterShip());
    // this.spaceShips.push(new BomberShip());
    // this.spaceShips.push(new Orzel3Ship());
  }

  assignPilot(spaceShip: SpaceShip) {
    spaceShip.pilot = this.selectedPilot;
    this.pilotRoom.pilotLeave();
  }

  deassignPilot(spaceShip: SpaceShip) {
    this.pilotRoom.pilotReturn(spaceShip.pilot);
    spaceShip.pilot = null;
  }
}
